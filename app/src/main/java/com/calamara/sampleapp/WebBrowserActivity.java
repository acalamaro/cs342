package com.calamara.sampleapp;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.TextView;

/**
 * Created by alcal on 4/8/2015.
 */
public class WebBrowserActivity extends Activity {
    Button goButton;
    Button backButton;
    TextView urlBar;
    WebView browser;
    WebSettings browserSettings;
    InputMethodManager imm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.web_activity);
        getActionBar().setDisplayHomeAsUpEnabled(true);

        goButton = (Button) findViewById(R.id.go_button);
        backButton = (Button) findViewById(R.id.back_button);
        urlBar = (TextView) findViewById(R.id.url_box);
        browser = (WebView) findViewById(R.id.webview);
        browser.setWebViewClient(new WebViewClient());
        browserSettings = browser.getSettings();
        browserSettings.setJavaScriptEnabled(true);
        browserSettings.setSupportZoom(true);

        imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);


        urlBar.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                boolean handled = false;
                if(actionId == EditorInfo.IME_ACTION_SEARCH){
                    go();
                    handled = true;
                }
                return handled;
            }
        });

        goButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                go();
            }
        });

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                browser.goBack();
            }
        });
    }

    protected void go(){
        imm.hideSoftInputFromWindow(urlBar.getWindowToken(), 0);
        String target = urlBar.getText().toString();
        if(!target.startsWith("http://")){
            target = "http://" + target;
        }
        browser.loadUrl(target);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.about_app) {
            Intent i = new Intent(this, AboutActivity.class);
            startActivity(i);
            overridePendingTransition(R.anim.slide_in_from_right, R.anim.slide_out_to_left);

        }
        if (id == android.R.id.home) {
            this.finish();
            overridePendingTransition(R.anim.slide_in_from_left, R.anim.slide_out_to_right);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onPause() {
        super.onPause();
        browser.onPause();
    }
    @Override
    protected void onResume(){
        super.onResume();
        browser.onResume();
    }
}
