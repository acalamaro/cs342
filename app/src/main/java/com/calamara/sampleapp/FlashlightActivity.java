package com.calamara.sampleapp;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.hardware.Camera;
import android.hardware.Camera.Parameters;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;
import android.widget.ToggleButton;


/**
 * Created by alcal on 4/5/2015.
 */
public class FlashlightActivity extends Activity {
    Camera cam;
    Parameters params;
    Boolean flashIsOn = false;
    ToggleButton lightSwitch;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.flashlight_activity);
        getActionBar().setDisplayHomeAsUpEnabled(true);

        loadCamera();

        lightSwitch = (ToggleButton) findViewById(R.id.togglebutton);
        lightSwitch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!flashIsOn){
                    flashOn();
                }else{
                    flashOff();
                }
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    protected void flashOn(){
        if(!flashIsOn){
            params = cam.getParameters();
            params.setFlashMode(Parameters.FLASH_MODE_TORCH);
            cam.setParameters(params);
            cam.startPreview();
            flashIsOn = true;
        }
    }

    protected void flashOff(){
        if(flashIsOn){
            params = cam.getParameters();
            params.setFlashMode(Parameters.FLASH_MODE_OFF);
            cam.setParameters(params);
            cam.stopPreview();
            flashIsOn = false;
        }
    }

    protected boolean loadCamera() {
        boolean hasFlash = getApplicationContext().getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA_FLASH);
        if (hasFlash){
            try {
                cam = Camera.open();
                params = cam.getParameters();
                return true;
            } catch (Exception e) {

                e.printStackTrace();
                return false;
            }

        }
        else{
            Toast toast = Toast.makeText(getApplicationContext(), "Flash Unavailable", Toast.LENGTH_SHORT);
            toast.show();
            return false;

        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.about_app) {
            Intent i = new Intent(this, AboutActivity.class);
            startActivity(i);
            overridePendingTransition(R.anim.slide_in_from_right, R.anim.slide_out_to_left);

        }
        if (id == android.R.id.home) {
            this.finish();
            overridePendingTransition(R.anim.slide_in_from_left, R.anim.slide_out_to_right);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
