package com.calamara.sampleapp;

import android.app.Activity;
import android.content.Intent;
import android.content.res.AssetFileDescriptor;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import java.io.IOException;

/**
 *  Exploring the media player
 */
public class MusicActivity extends Activity implements View.OnClickListener{

    public MediaPlayer mp;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_music);

        Button play = (Button)this.findViewById(R.id.playButton);
        play.setOnClickListener(this);

        Button stop = (Button)this.findViewById(R.id.stopButton);
        stop.setOnClickListener(this);

        mp = new MediaPlayer();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.about_app) {
            Intent i = new Intent(this, AboutActivity.class);
            startActivity(i);
            overridePendingTransition(R.anim.slide_in_from_right, R.anim.slide_out_to_left);

        }
        if (id == android.R.id.home) {
            this.finish();
            overridePendingTransition(R.anim.slide_in_from_left, R.anim.slide_out_to_right);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void onClick(View view) {
        // Switch on the button name
        switch (view.getId()) {
            case(R.id.playButton):
                // Make sure song doesn't play over another song
                if (mp.isPlaying()) {
                    mp.pause();
                }
                // Try to open the mp3 and play it
                try {
                    mp.reset();
                    AssetFileDescriptor afd;
                    afd = getAssets().openFd("song.mp3");
                    mp.setDataSource(afd.getFileDescriptor(), afd.getStartOffset(), afd.getLength());
                    mp.prepare();
                    mp.start();
                } catch (IllegalStateException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                break;
            // Stop the music
            case(R.id.stopButton):
                if(mp.isPlaying()) {
                    mp.stop();
                }
                break;
        }
    }

}
